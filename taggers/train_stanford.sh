KEY=$1
TRAIN_DATA=$2
MODELS=models
PROPS=stanford-postagger-full/childes.prop

export CLASSPATH=stanford-postagger-full/stanford-postagger.jar

java edu.stanford.nlp.tagger.maxent.MaxentTagger -prop $PROPS -model $MODELS/$KEY.stanford.model -trainFile $TRAIN_DATA

MODELNAME=$1
TRAIN_DATA=$2
echo Training the Brill tagger, using the model name \"$MODELNAME\" and training file \"$TRAIN_DATA\"
echo "Prepping the data..."
scripts/prep_brill.sh $MODELNAME $TRAIN_DATA $TRAIN_DATA
echo "Learning lexical rules..."
scripts/train1_brill.sh $MODELNAME $TRAIN_DATA $TRAIN_DATA
echo "Learning contextual rules..."
scripts/train2_brill.sh $MODELNAME $TRAIN_DATA $TRAIN_DATA
echo "All training done"

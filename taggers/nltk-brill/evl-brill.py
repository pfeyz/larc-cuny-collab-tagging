"""

        This script running brill tagger and output confusion matrix as csv files
        argv1: input, corpus root
        argv2: input, traning file ID
        argv3: input, testing file ID
        argv4: output, csv file ID
        $ python brill.py argv1 argv2 argv3 argv4
        do in a bash:
        for i in $(seq 0 9); do python evl-brill.py Valian/ train$i.txt test$i.txt cm$i.csv; done

"""

import numpy as np
import sys
import csv
import nltk.tag
from nltk.tag import brill
from nltk.corpus.reader.tagged import TaggedCorpusReader

def train_brill_tagger(train_data):
    # Modules for creating the templates.
    from nltk.tag import UnigramTagger
    from nltk.tag.brill import SymmetricProximateTokensTemplate, ProximateTokensTemplate
    from nltk.tag.brill import ProximateTagsRule, ProximateWordsRule
    from nltk.tag.brill import FastBrillTaggerTrainer
    unigram_tagger = UnigramTagger(train_data)
    templates = [SymmetricProximateTokensTemplate(ProximateTagsRule, (1,1)),
                            SymmetricProximateTokensTemplate(ProximateTagsRule, (2,2)),
                            SymmetricProximateTokensTemplate(ProximateTagsRule, (1,2)),
                            SymmetricProximateTokensTemplate(ProximateTagsRule, (1,3)),
                            SymmetricProximateTokensTemplate(ProximateWordsRule, (1,1)),
                            SymmetricProximateTokensTemplate(ProximateWordsRule, (2,2)),
                            SymmetricProximateTokensTemplate(ProximateWordsRule, (1,2)),
                            SymmetricProximateTokensTemplate(ProximateWordsRule, (1,3)),
                            ProximateTokensTemplate(ProximateTagsRule, (-1, -1), (1,1)),
                            ProximateTokensTemplate(ProximateWordsRule, (-1, -1), (1,1))]

    trainer = FastBrillTaggerTrainer(initial_tagger=unigram_tagger,
                                   templates=templates, trace=3,
                                   deterministic=True)
    brill_tagger = trainer.train(train_data, max_rules=10)
    return brill_tagger

def test_brill_tagger(root, train_fileID, test_fileID):
    corpus = TaggedCorpusReader(root, '.*')
    train_data = list(corpus.tagged_sents(train_fileID))
    # print len(train_data)
    test_data = list(corpus.tagged_sents(test_fileID))
    bt = train_brill_tagger(train_data)
    print 'Accuracy of Brill Tagger:', bt.evaluate(test_data)
    print 'Testing Brill tagger on', len(test_data), 'sentences'

# if __name__ == "__main__":
    # test_brill_tagger(sys.argv[1], sys.argv[2], sys.argv[3])


# def matrix(root, train_fileID, test_fileID):
    # test_brill_tagger(root, train_fileID, test_fileID)

    test_tag_list = []
    tagged_file = corpus.sents(test_fileID)
    # print tagged_file


# test_brill_tagger('VVV/', 'train0.txt', 'test0.txt')

    for line in tagged_file:
        tagged_sents = bt.tag(line)
        for sent in tagged_sents:
            test_tag_list.append(sent[1])

    gold_tag_list = []
    for line in test_data:
        for tags in line:
            gold_tag_list.append(tags[1])

    cm = nltk.ConfusionMatrix(gold_tag_list, test_tag_list)
    return cm

def precision(root, gold, test):
    m = test_brill_tagger(root, gold, test)
    precision_list = []
    for count_list in m._confusion:
        p_max_count = max(count_list)
        p_all_count = sum(count_list)
        precision = p_max_count * 100/float(p_all_count)
        precision_list.append(round(precision, 2))

    return precision_list

def precision_and_recall(root, gold, test, output_f):
    m = test_brill_tagger(root, gold, test)
    matrix_array = np.array(m._confusion)
    max_p_count = matrix_array.max(axis=1)
    total_p = matrix_array.sum(axis = 1)
    precision = np.divide(max_p_count*100, total_p, dtype = float)

    for count_list in m._confusion:
        count_list.append(sum(count_list))
    sum_matrix_array= np.array(m._confusion)

    max_r_count = matrix_array.max(axis = 0)
    total_r = matrix_array.sum(axis = 0)
    recall = np.divide(max_r_count*100 , total_r, dtype = float)
    total_r_list = total_r.tolist()
    recall_list = recall.tolist()

    data = m._confusion

    for index, row in enumerate(data):
        row.append(round(precision[index],2))
    data.append([ elem for elem in total_r_list])
    data.append([ '%.2f' % elem for elem in recall_list])

    reference_tag_list = m._values
    reference_tag_list.append("Total")
    reference_tag_list.append("Recall")

    for index, row in enumerate(data):
        if index < len(data):
            row.insert(0, reference_tag_list[index])

    reference_tag_list.insert(0,'TAG')
    reference_tag_list[-1] = 'Precision'
    data.insert(0, reference_tag_list)

    with open(output_f, 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(data)

# precision_and_recall('VVV/', 'train0.txt', 'test0.txt', 'csv0.csv')

if __name__ == "__main__":
    precision_and_recall(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])


TEST_CORPUS=$2
KEY=$1
UNTAGGED_TEST_CORPUS=${TEST_CORPUS%.txt}.untagged.txt
BASELINEOUTPUT=${TEST_CORPUS%.txt}.baselinetagged.txt
OUTPUT=${TEST_CORPUS%.txt}.brilltagged.txt
export PATH=brill/Learner_Code:brill/Utilities:brill/Bin_and_Data/:$PATH
BIN_PATH=brill/Bin_and_Data
MODELS=models

# Untag the test corpus
tagged-to-untagged.prl < $TEST_CORPUS > $UNTAGGED_TEST_CORPUS

# Tag
cp $BIN_PATH/tagger $BIN_PATH/final-state-tagger $BIN_PATH/start-state-tagger .
# Usage: tagger LEXICON YOUR-CORPUS BIGRAMS LEXICALRULEFULE CONTEXTUALRULEFILE
# Baseline tagging
./tagger $MODELS/$KEY.brill.FINAL.LEXICON $UNTAGGED_TEST_CORPUS $MODELS/$KEY.brill.BIGBIGRAMLIST $MODELS/$KEY.brill.LEXRULEOUTFILE $MODELS/$KEY.brill.CONTEXT-RULEFILE -S > $BASELINEOUTPUT
# Full tagging
./tagger $MODELS/$KEY.brill.FINAL.LEXICON $UNTAGGED_TEST_CORPUS $MODELS/$KEY.brill.BIGBIGRAMLIST $MODELS/$KEY.brill.LEXRULEOUTFILE $MODELS/$KEY.brill.CONTEXT-RULEFILE > $OUTPUT

# Check accuracy
echo 'Baseline accuracy:'
comparitor.prl $BASELINEOUTPUT $TEST_CORPUS $MODELS/$KEY.brill.FINAL.LEXICON | tail -6
echo
echo 'Final accuracy:'
comparitor.prl $OUTPUT $TEST_CORPUS $MODELS/$KEY.brill.FINAL.LEXICON | tail -6

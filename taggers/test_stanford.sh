KEY=$1
TESTFILE=$2
MODELS=models
RAWTESTFILE=${TESTFILE%.txt}.untagged.txt
OUTPUT=${TESTFILE%.txt}.stanfordtagged.txt
PROPS=stanford-postagger-full/childes.prop

export CLASSPATH=stanford-postagger-full/stanford-postagger.jar
export PATH=brill/Utilities:$PATH # We use the Brill tagged-to-untagged

# Get a score
java -Xmx1g edu.stanford.nlp.tagger.maxent.MaxentTagger -prop $PROPS -model $MODELS/$KEY.stanford.model -testFile $TESTFILE | tail -7

# Give tagged output
tagged-to-untagged.prl < $TESTFILE > $RAWTESTFILE
java -Xmx1g edu.stanford.nlp.tagger.maxent.MaxentTagger -prop $PROPS -model $MODELS/$KEY.stanford.model -textFile $RAWTESTFILE > $OUTPUT

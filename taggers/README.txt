
This packages together the Brill, ACOPOST t3, and Stanford taggers for
evaluation. To use them, do the following:

Before you run the taggers at all:
./make_taggers.sh
This will compile all the taggers on your system. Working copies of gcc
and javac are assumed.

From there, you can train and test each tagger. When you train the tagger,
you need to provide a name for the model so it can be used again in tagging.
For example, let's say I'm working on the new tagset Eve data. I might
train the t3 tagger as follows:
./train_t3.sh eve-new eve-new-training.txt
Where "eve-new" is the name I'm giving to the model, and eve-new-training.txt
is the training data. No matter which tagger you use, the data needs to
be in the slash format, i.e.:
you/PRO have/VB another/QNT cookie/N right/ADV on/PRE the/DET table/N ./.
The scripts take care of any conversions needed from that format.

When it's time to test, using the model name allows you to use the model you
trained. So now when I want to tag data using the model trained from
eve-new-training.txt, I do the following:
./test_t3.sh eve-new eve-new-testing.txt
Here we assume eve-new-testing.txt is a file in the same format as training
but contains a different set of data.

We used t3 as an example, but things apply equally to the other two taggers:
./train_brill.sh eve-new eve-new-training.txt
./test_brill.sh eve-new eve-new-testing.txt
./train_stanford.sh eve-new eve-new-training.txt
./test_stanford.sh eve-new eve-new-testing.txt

Keep in mind while the name of a model may be the same across taggers,
there isn't anything in common really- a t3 model with the name "eve-new"
and a Stanford model with the name "eve-new" are two totally different things.

So if you want to try out all three taggers, train and test each using the
training data. When you test, in addition to getting the accuracy back,
you can look at the tagged file. If the file used in testing was
eve-new-testing.txt, the output will have the name eve-new-testing.t3tagged.txt
(or equivalently for Brill or Stanford).

Right now this setup assumes we are testing with an already tagged file. I'll
also add tagging scripts that just take untagged data.

If you want to recompile the taggers, you can run clean_taggers.sh and then
make_taggers.sh again. If you want to toss out all the models you've made,
you can run clean_models.sh.

Let me know if you have questions!
-c$ (Constantine Lignos, 11/17/2010)

#!/usr/bin/env bash

KEY=$1
TRAIN_CORPUS=$2
BIG_CORPUS=$3
export PATH=brill/Learner_Code:brill/Utilities:brill/Bin_and_Data/:$PATH
MODELS=models

# Prep work
cp $TRAIN_CORPUS $MODELS/$KEY.brill.TRAIN # Will be used for final lexicon
divide-in-two-rand.prl $MODELS/$KEY.brill.TRAIN1 $MODELS/$KEY.brill.TRAIN2 < $TRAIN_CORPUS
tagged-to-untagged.prl < $BIG_CORPUS > $MODELS/$KEY.brill.UNTAGGED
cat $MODELS/$KEY.brill.UNTAGGED | wordlist-make.prl | sort -k 11 -rn | awk '{print $1}' > $MODELS/$KEY.brill.BIGWORDLIST
cat $MODELS/$KEY.brill.UNTAGGED | bigram-generate.prl | awk '{print $1,$2}' > $MODELS/$KEY.brill.BIGBIGRAMLIST



#!/usr/bin/env bash

KEY=$1
TRAIN_CORPUS=$2
BIG_CORPUS=$3
export PATH=brill/Learner_Code:brill/Utilities:brill/Bin_and_Data/:$PATH
BIN_PATH=brill/Bin_and_Data
MODELS=models

# Handle second part of training- contextual cues
# Make the training lexicons
make-restricted-lexicon.prl < $MODELS/$KEY.brill.TRAIN1 > $MODELS/$KEY.brill.TRAINING.LEXICON
make-restricted-lexicon.prl < $MODELS/$KEY.brill.TRAIN > $MODELS/$KEY.brill.FINAL.LEXICON

# Tag
# Copy tagger executable and then make corpus and tag
tagged-to-untagged.prl < $MODELS/$KEY.brill.TRAIN2 > $MODELS/$KEY.brill.UNTAGGED2
# Move to taggers to here and tag come back
cp $BIN_PATH/tagger $BIN_PATH/final-state-tagger $BIN_PATH/start-state-tagger .
./tagger $MODELS/$KEY.brill.TRAINING.LEXICON $MODELS/$KEY.brill.UNTAGGED2 $MODELS/$KEY.brill.BIGBIGRAMLIST $MODELS/$KEY.brill.LEXRULEOUTFILE /dev/null -w $MODELS/$KEY.brill.BIGWORDLIST -S > $MODELS/$KEY.brill.DUMMY-TAGGED-CORPUS 2> /dev/null

# Learn contextual rules
contextual-rule-learn $MODELS/$KEY.brill.TRAIN2 $MODELS/$KEY.brill.DUMMY-TAGGED-CORPUS $MODELS/$KEY.brill.CONTEXT-RULEFILE $MODELS/$KEY.brill.TRAINING.LEXICON > /dev/null
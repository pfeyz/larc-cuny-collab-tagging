#!/usr/bin/env bash

KEY=$1
TRAIN_CORPUS=$2
BIG_CORPUS=$3
export PATH=brill/Learner_Code:brill/Utilities:brill/Bin_and_Data/:$PATH
MODELS=models

# Train on the first half of the data
cat $MODELS/$KEY.brill.TRAIN1 | word-tag-count.prl | sort -k 2 -rn > $MODELS/$KEY.brill.TRAIN1.SMALLWORDTAGLIST
unknown-lexical-learn.prl $MODELS/$KEY.brill.BIGWORDLIST $MODELS/$KEY.brill.TRAIN1.SMALLWORDTAGLIST $MODELS/$KEY.brill.BIGBIGRAMLIST 300 $MODELS/$KEY.brill.LEXRULEOUTFILE > /dev/null

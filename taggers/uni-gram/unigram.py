# nltk uni-gram tagger
# 20160530
"""
       do in a bash:
        for i in $(seq 0 9); do python unigram.py Valian/ train$i.txt test$i.txt; done >> Valian.txt


"""
import sys
import nltk.tag
from nltk.corpus.reader.tagged import TaggedCorpusReader
# from nltk.corpus import brown
# brown_tagged_sents = brown.tagged_sents(categories='news')
# print brown_tagged_sents
# brown_sents = brown.sents(categories='news')
# unigram_tagger = nltk.UnigramTagger(brown_tagged_sents)
# print unigram_tagger.tag(brown_sents[2007])
def train_unigram_tagger(train_data):
    from nltk.tag import UnigramTagger
    unigram_tagger = UnigramTagger(train_data)
    return unigram_tagger

def test_unigram_tagger(root, train_fileID, test_fileID):
    corpus = TaggedCorpusReader(root, '.*')
    train_data = list(corpus.tagged_sents(train_fileID))
    test_data = list(corpus.tagged_sents(test_fileID))
    ut = train_unigram_tagger(train_data)
    print 'Accuracy of UnigramTagger:', ut.evaluate(test_data)
    # for line in test_data:
        # print line
        # print ut.tag(line)
# test_unigram_tagger('CHI-set/', 'train1.txt', 'test1.txt' )
if __name__ == "__main__":
    test_unigram_tagger(sys.argv[1], sys.argv[2], sys.argv[3] )


KEY=$1
TESTFILE=$2
MODELS=models
COOKEDTESTFILE=${TESTFILE%.txt}.cooked.txt
RAWTESTFILE=${TESTFILE%.txt}.untagged.txt
MODELFILE=$MODELS/$KEY.t3.ngram
LEXICON=$MODELS/$KEY.t3.lex
OUTPUT=${TESTFILE%.txt}.t3tagged.txt

export PATH=acopost/bin:$PATH

# Convert testing data to cooked and raw
wsj2cooked.pl < $TESTFILE > $COOKEDTESTFILE 2> /dev/null
cooked2raw.pl < $COOKEDTESTFILE > $RAWTESTFILE 2> /dev/null

# Test
t3 $MODELFILE $LEXICON $RAWTESTFILE > $OUTPUT 2> /dev/null

# Evaluate
evaluate.pl -v $COOKEDTESTFILE $OUTPUT

#t!/usr/bin/env bash

KEY=$1
TRAINFILE=$2
MODELS=models
COOKEDTRAINFILE=${TRAINFILE%.txt}.cooked.txt
MODELFILE=$MODELS/$KEY.t3.ngram
LEXICON=$MODELS/$KEY.t3.lex

export PATH=acopost/bin:$PATH

# Convert training data to cooked
wsj2cooked.pl < $TRAINFILE > $COOKEDTRAINFILE

# Train ngrams
cooked2ngram.pl < $COOKEDTRAINFILE > $MODELFILE

# Make lexicon
cooked2lex.pl < $COOKEDTRAINFILE > $LEXICON

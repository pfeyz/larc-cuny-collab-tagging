===============================
 The Corpus Correction Process
===============================

Human Annotator Correction Pass 1 (first check)
===============================================

Creates a list of changes to make to the CHILDES Corpora. This is implemented as
a python script written by a former intern::

    https://github.com/pfeyz/tagchecker

The script reads XML corpora files as input and generates a list of corrections
that should be applied, stored in one CSV file per input XML file.

- XML files stored at: cuny-collab-tagging / corpora / valian / xml / 

- The format of csv files is:
Line,Speaker,Utterance,Tags,Incorrect Tag,Correct Tag,Notes
25,*MOT,do whatever ?,mod|do pro:wh|whatever ? ,mod|do,v|do,note

The flaw in this step is that given an utterance like this::

  pro|I v|want part|to v|present pro|you det|a v|present

and a annotator who decides to change the second v|present to n|present, the
correction stored only contains the following information::

  v|present -> n|present

which contains no information about which occurrence of v|present to
target. This means that a naive application of this correction will overcorrect
in this case, changing both instances of present from v to n.

Human Annotator Correction Pass 2 (double/second check)
=======================================================

Human annotators approve or modify all corrections from Pass 1. Implemented as a
Django web application::

    https://bitbucket.org/pfeyz/django_childes_corrector

The result of this pass is a single csv file with similar structure to the Pass
1 files but with information about the source file, corpora and user who
corrected it. The overcorrection problem mentioned in Pass 1 is still unresolved
at this stage.

Correction Application
======================

Applies the Human Annotator Corrections to a Corpus.

- FIXME: what happens here with the clitics and compound words?

"""Apply a directory full of corrections to a corpus, or analyze how cleanly
this application would go.

Usage:
  correction_tools.py apply <corrections-dir> <corpus-dir> <output-dir>
  correction_tools.py analyze_all_corpora <corrections-dir> <corpora-dir>
  correction_tools.py analyze_corpus <corrections-dir> <corpus-dir>
  correction_tools.py stripped <corrections-dir> <corpus-dir> <output-dir>

  <corrections-dir> the directory where the correction files live
  <corpus-dir>      the directory where the corpus files live.
  <corpora-dir>     a directory containing multiple copies of a corpora. these
                    are potential targets for the corrections in correction-dir.

  The correction and corpus files should be named in such a way that they can be
  paired by stripping their file extensions off.

"""
from __future__ import print_function

import csv
import warnings
import re
import sqlite3
from collections import namedtuple, Counter
from glob import glob
from os.path import basename, splitext, dirname
from os.path import join as pjoin

from docopt import docopt

from talkbank_parser import MorParser, MorToken, MalformedTokenString

#
import logging
logging.basicConfig(level=logging.DEBUG, filemode='w',
                    # filename = 'correction_tools.log'
                    filename='{}.log'.format(__name__))
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)

# A named tuple storing a single correction.
#
# >>> c = Correction('paul', 'valian', '.../valian01a.txt', ..etc...)
# >>> c.corpus == 'valian'
# True
#
Correction = namedtuple('Correction',
                        ('user corpus filename uid speaker speaker_tier '
                         'mor_tier error replacement notes'))

"""

"already corrected": Ew|Et is not found in utterance, but Tw|Tt is. This
suggests that the utterance was already corrected somehow.

"""

class CorrectionFailure(Exception):
    pass

class UtteranceNotFound(CorrectionFailure):
    """The uid targeted by the Correction does not match that found in the
    Corpus.

    """
    def __init__(self, uid):
        self.uid = uid

class TokenNotFound(CorrectionFailure):
    """Given a Correction E -> C, E is not found in the target"""
    def __init__(self, uid, target):
        super(TokenNotFound, self).__init__(uid, target)
        self.uid = uid
        self.target = target

class AlreadyCorrected(CorrectionFailure):
    """Given a Correction E -> C, E is not found in the target, but C is."""
    def __init__(self, uid, found):
        super(AlreadyCorrected, self).__init__(uid, found)
        self.uid = uid
        self.found = found

class MultipleFound(CorrectionFailure):
    """Given a Correction E -> C, E occurs multiple times in utterance"""
    def __init__(self, uid, found):
        super(MultipleFound, self).__init__(uid, found)
        self.uid = uid
        self.found = found

class MalformedCorrection(CorrectionFailure):
    def __init__(self, uid, correction):
        super(MalformedCorrection, self).__init__(uid, correction)
        self.uid = uid
        self.correction = correction

class CorrectionReport(object):
    max_lengths = {}
    def __init__(self, **kwargs):
        filename = kwargs['corpus_fn']
        self.target_dir = dirname(filename)
        self.target_file = normalized_filename(filename)
        self.corpus_length = kwargs['corpus_length']
        self.num_corrections = kwargs['num_corrections']
        self.fail_counts_by_type = kwargs['fail_counts_by_type']
        self.max_lengths[self.target_file] = max(self.corpus_length,
                                                 self.max_lengths.get(self.target_file, 0))

    @property
    def accuracy(self):
        return round(1 - (float(self.num_failed) / self.num_corrections),
                     3)

    @property
    def num_failed(self):
        return sum(count
                   for ftype, count in
                   self.fail_counts_by_type.items()
                   if ftype != 'MultipleFound')

    def score(self):
        errors = self.num_failed
        full_len = self.max_lengths[self.target_file]
        errors += full_len - self.corpus_length
        return 1 - (errors / float(full_len))

    def __repr__(self):
        return "CorrectionReport({}, {}/{}, {}%)".format(
            self.target_file, self.num_failed, self.num_corrections,
            self.accuracy)

class RemoveUtterance(object):
    """A sentinel class that will serve as a flag for when an utterance should be
    removed.

    """
    def __init__(self):
        raise NotImplementedError("RemoveUtterance is not meant to be instantiated.")

def remove_unknowns(tokens):
    """Accepts a list of MorTokens, returning a list of MorTokens 
    with all unknowns (tagged as 'unk') removed. 
    If the entire utterance should be elided from the output, 
    will return RemoveUtterance instead of a list.
    """
    '''
    tokens = [token
              for token in tokens
              if token.pos != 'unk']
    
    if len(tokens) < 1 or tokens[0].is_punct():
        return RemoveUtterance
    
    return tokens
    '''
    return remove_utts('pos', 'unk', tokens)

def remove_utts(tag, bad_tag, tokens):
    """Accepts list of MorTokens. Checks each token for given tag.
    Those that are tagged as something undesirable are removed,
    and the remaining are returned in a new list.
    If the entire utterance should be elided from the output, 
    will return RemoveUtterance instead of a list.
    """
    if tag == 'pos':
        tokens = [token
              for token in tokens
              if token.pos != bad_tag]
    elif tag == 'stem':
        tokens = [token
              for token in tokens
              if token.stem != bad_tag]
    elif tag == 'word':
        tokens = [token
              for token in tokens
              if token.word != bad_tag]
    else:
        raise NotImplementedError("Attribute {} not defined for remove_utts".format(tag))
    if len(tokens) < 1 or tokens[0].is_punct():
        return RemoveUtterance
    return tokens

def normalized_filename(filename):
    """Returns a filename with path and extension stripped"""
    return splitext(basename(filename))[0]

def normalized_mor_tier(utterance):
    """Takes a list of MorToken (defined in talkbank_parser module) objects and
    returns a string representation approximating CHA's MOR tier.

    """
    # TODO: handle clitics and compounds correctly
    mor_tier = [str(mor_token).split('/')[1]  # discard wordforms
                for mor_token in utterance]
    return ' '.join(mor_tier)

def pair_files(corrections_dir, corpus_dir):
    """
    Returns ([(String, String)], # paired files
             [String])           # unpaired files

    Returns a 2-tuple of 1: a list of pairs (correction, corpus) of matching
    filenames, and 2: the filenames that weren't matched to anything. 'Match'
    means two files have the same name excluding path & extension.
    """
    corrections = glob(pjoin(corrections_dir, '*'))
    corpora = glob(pjoin(corpus_dir, '*'))


    pairs = [(correction, corpus)
             for correction in corrections
             for corpus in corpora
             if normalized_filename(correction) == normalized_filename(corpus)]
    paired_corrections = [pair[0] for pair in pairs]
    paired_corpora = [pair[1] for pair in pairs]
    unpaired = [correction
                for correction in corrections
                if not paired_corrections.count(correction)]
    unpaired.extend([
        corpus
        for corpus in corpora
        if not paired_corpora.count(corpus)
    ])
    return pairs, unpaired


def match(items, targets, start=0):
    """Returns the first index where items is equal it a subsequence of targets, or
    None if there's no match. Searching starts at index `start` if specified.

    Args:

    items: a list of MorTokens to search for.
    targets: a list of MorTokens to match against.
    start: an int specifying what index to begin searching against.

    """
    start_index = None
    try:
        start_index = targets.index(items[0], start)
        for offset, token in enumerate(items[1:]):
            targets.index(token, start_index + start + offset + 1)
    except ValueError:
        return None
    return start_index

def apply_correction(correction, utterance):
    """Modifies `utterance` in place according to the change dictated by
    correction.

    Args:

    correction: a Correction namedtuple that targets `utterance`.

    utterance: a list of MorToken objects. The MorTokens themselves are not
    mutated, only added or deleted from the list.

    Returns nothing, raises a subclass of CorrectionFailure on error.

    """
    uid, speaker, tokens = utterance

    if correction.speaker != speaker:
        raise UtteranceNotFound(uid)

    try:
        error = [MorToken.from_string(part)
                 for part in correction.error.strip().replace('~', ' ').split(' ')]
        replacement = [MorToken.from_string(part)
                       for part in correction.replacement.strip().replace('~', ' ').split(' ')]
    except MalformedTokenString as e:
        raise MalformedCorrection(uid, correction)

    match_offset = match(error, tokens)
    if match_offset is None:
        if match(replacement, tokens) is not None:
            logger.warning('Error and replacement were swapped on input! (%s vs %s)', error, replacement)
            error, replacement = replacement, error  # doesn't do anything now possibly?
            raise AlreadyCorrected(uid, correction.replacement)
        else:
            logger.warning('Error and replacement not found in utterance!')
            raise TokenNotFound(uid, correction.error)

    if match(error, tokens, match_offset + 1) is not None:
        raise MultipleFound(uid, correction.error)

    if len(replacement) == 1 and len(error) == 1 and \
       (replacement[0].stem == error[0].stem):
        # if the annotator did not change the word's stem, keep the wordform
        # from the old utterance. wordforms were not provided by the
        # tag-checking annotators. this is problematic when the stem *is*
        # changed, since it's unclear whether to change the wordform to the new
        # stem, possibly losing word morphology, or to keep the wordform the
        # same, which is the wrong thing to do when the annotator changes the
        # stem word to an entirely different lexical entry...
        replacement[0].word = tokens[match_offset].word

    for _ in error:
        tokens.pop(match_offset)

    for token in replacement[::-1]:
        tokens.insert(match_offset, token)

def apply_corrections(corrections, corpus):
    """Applies the list of correction to the list of utterances in-place.

    Args:

    correction: a list of Correction namedtuples.

    corpus: A list of Utterances, where an Utterance is a list of
    MorTokens. These inner-lists will be mutated in order to reflect the changes
    specified by the Correction tuples.

    Returns a list of CorrectionFailure subclass exceptions that were raised
    during application attempts.

    """
    accepted = []
    failures = []

    for line in corrections:
        if len(line) == 9:
            line.append('')
        correction = Correction(*line)

        try:
            utterance = corpus[int(correction.uid)]
        except IndexError:
            failures.append(UtteranceNotFound(correction.uid))
            continue

        try:
            apply_correction(correction, utterance)
        except MalformedCorrection as error:
            corex = error.correction
            print('malformed correction {fn}:{uid} {err} -> {corr}'.format(
                fn=corex.filename,
                uid=corex.uid,
                err=corex.error,
                corr=corex.replacement))
            failures.append(error)
        except CorrectionFailure as error:
            failures.append(error)
            continue

        accepted.append(correction)
    return accepted, failures

def read_corpus(filename, parser=MorParser("{http://www.talkbank.org/ns/talkbank}")):
    assert filename.endswith('.xml')
    corpus = parser.parse(filename)
    return list(corpus)

def read_corrections(filename):
    def pad(line):
        """ Adds a note to line if its missing """
        if len(line) == 9: return line + ['']
        else: return line
    with open(filename, 'r') as corrections_fh:
        return [Correction(*pad(line)) for line in
                csv.reader(corrections_fh, delimiter='\t')]

def apply_all_corrections(corrections_dir, corpus_dir):
    pairs, unpaired = pair_files(corrections_dir, corpus_dir)

    if len(unpaired) > 0:
        warnings.warn('{} unpaired correction & corpus files: \n\t{}'.format(
            len(unpaired), '\n\t'.join(sorted(unpaired))))

    for correction_fn, corpus_fn in sorted(pairs):
        corpus = read_corpus(corpus_fn)
        corrections = read_corrections(correction_fn)
        if len(corrections) == 0:
            warnings.warn('Empty corrections file: {}'.format(correction_fn))
            continue
        accepted, failures = apply_corrections(corrections, corpus)
        yield corpus_fn, corpus, accepted, failures

def collect_best(acc, item):
    fn, cid, total, errors, percentage = item
    if cid not in acc:
        acc[cid] = (percentage, fn, errors)
    return acc

def analyze_failures(corrections_dir, corpora_dir):
    rejections = []
    parser = MorParser("{http://www.talkbank.org/ns/talkbank}")
    for corpus_dir in glob(corpora_dir + "*xml*"):
        pairs, _ = pair_files(corrections_dir, corpus_dir)
        for correction_fn, corpus_fn in pairs:
            corrections = list(csv.reader(open(correction_fn), delimiter='\t'))
            corpus = list(parser.parse(corpus_fn))
            acc, rej = apply_corrections(corrections, corpus, corpus_dir, corpus_fn)
            print(corpus_fn, len(acc))
            rejections.extend(rej)
    return rejections

def make_stripped_corpus(corrections_dir, corpus_dir, output_dir):
    """Creates a copy of the `corpus` directory with all utterances targeted by
    `corrections` removed. The result is a corpus that we claim not to have any
    errors.

    """
    pairs, unpaired = pair_files(corrections_dir, corpus_dir)
    print(sorted(unpaired))
    parser = MorParser("{http://www.talkbank.org/ns/talkbank}")
    readme = open(pjoin(output_dir, 'README.txt'), 'w')
    for corrections_fn, corpus_fn in sorted(pairs):
        output = open(pjoin(output_dir, normalized_filename(corpus_fn) + '.txt'), 'w')
        assert corpus_fn.endswith('.xml'), corpus_fn
        corpus = list(parser.parse(corpus_fn))
        corrections = [Correction(*line[:9] + [' '])
                       for line in csv.reader(open(corrections_fn),
                                              delimiter='\t')]
        uids = dict((int(c.uid.rstrip('u')), c) for c in corrections)
        count = 0
        for uid, utterance in enumerate(corpus):
            if uid in uids:
                count += 1
                # print('SKIP', uids[uid], utterance)
            else:
                output.write(str(utterance[0]) + ' ')
                output.write(str(utterance[1]) + ' ')
                output.write(str(' '.join(map(str, utterance[2]))) + '\n')
        output.close()
        report = '{}: excluded {}/{}'.format(normalized_filename(corpus_fn), count, len(corpus))
        print(report)
        readme.write(report + '\n')
    readme.write("""
The following files were excluded from this corpus:
{}""".format('\n'.join(sorted(unpaired))))
    readme.close()

def calculate_bests(reports):
    from collections import defaultdict
    bests = {}
    for report in reports:
        print(report.target_dir, report.num_failed)
        target_file = report.target_file
        if target_file not in bests:
            bests[target_file] = report
            continue
        best_so_far = bests[target_file]
        if report.num_failed < best_so_far.num_failed:
            print(report, 'beat', best_so_far)
            bests[target_file] = report
    return bests

def analyze_corpus(correction_dir, corpus_dir):
    results = apply_all_corrections(args['<corrections-dir>'], corpus_dir)
    reports = []
    for fn, utts, accepted, failures in results:
        print('=' * 8, fn)
        # for failure in failures:
        #     if type(failure) is TokenNotFound:
        #         print(failure.__repr__(), utts[int(failure.uid.lstrip('u'))])
        #     pass
        reports.append(CorrectionReport(
            corpus_fn=fn,
            num_corrections=len(accepted) + len(failures),
            corpus_length=len(utts),
            fail_counts_by_type=Counter([type(f).__name__
                                         for f in failures])))
    return reports


if __name__ == "__main__":
    args = docopt(__doc__, version='0.1')
    if args['apply']:
        results = apply_all_corrections(args['<corrections-dir>'], args['<corpus-dir>'])
        output_dir = args['<output-dir>']
        for fn, utts, accepted, failures in results:
            outfile = pjoin(output_dir, normalized_filename(fn) + '.txt')
            with open(outfile, 'w') as fh:
                for uid, speaker, tokens in utts:
                    tokens = remove_unknowns(tokens)
                    if tokens is RemoveUtterance:
                        continue
                    fh.write("{}\t{}\t{}\n".format(
                        uid, speaker, ' '.join([str(t) for t in tokens])
                    ))
            print('wrote', outfile)

    elif args['stripped']:
        make_stripped_corpus(args['<corrections-dir>'], args['<corpora-dir>'], args['<output-dir>'])
    elif args['analyze_corpus']:
        analyze_corpus(args['<corrections-dir>'], args['<corpus-dir>'])
    elif args['analyze_all_corpora']:
        from pprint import pprint
        reports = []
        for corpus_dir in glob(args['<corpora-dir>'] + '/*xml*'):
            reports.extend(analyze_corpus(args['<corrections-dir>'], corpus_dir))
        pprint(reports)
        pprint(calculate_bests(reports))

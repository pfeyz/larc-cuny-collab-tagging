"""An evaluation tool for analyzing 'stem changes' in csv transcripts.
For example: n|doe-PL -> mod|do&3S

Generates a dict containing (error, fix) tuples against their frequencies.
Prints a summary of this dict, including a total count of stem changes.

As a program, can take any number of filenames to summarize together.
At 0 arguments, defaults to summarizing all of the files found in
/correction-data/second-check-output-csv/
"""

import csv, re, sys, collections
from glob import glob
from test_correction_tools import string_to_utterance

def all_freqs():
    """Returns a list of frequency dicts generated using stems_changed.
    """
    fnames = glob('../correction-data/second-check-output-csv/[0-1][1-8]*')
    freqs_list = []
    for fname in fnames:
        freqs_list.append(stems_changed(fname))
    return freqs_list

def freq_add(freqs, k, v):
    """A simple helper method. If key k is in dict freqs, freqs[k] += v.
    If not, freqs[k] is assigned the value v.
    """
    try:
        freqs[k] += v
    except KeyError:
        freqs[k] = v

def look_freqs(freqs):
    """A void method that 'reads' a frequency dict.
    Prints out every tuple of error/fix and the number of times it occurs,
    sorted by number of occurrences. Also prints a total count of fixes.
    """
    for tup, ct in sorted(freqs.items(), key=lambda x:x[1], reverse=True):
        print "{} occurs {} times".format(tup, ct)
    c = collections.Counter(freqs)
    count = sum(c.values())
    print "Total of {} stem changes identified.".format(count)

def summed_dict(freqs_list):
    """Takes a list of frequency dicts.
    Returns a dict updated with every entry among every dict in the list.
    """
    sd = freqs_list[0]
    for i in range(1, len(freqs_list)):
        for k in freqs_list[i]:
            freq_add(sd, k, freqs_list[i][k])
    return sd

def stems_changed(filename):
    """For a csv under filename, reads the file, pulls out any instances of
    stems being changed, and keeps a running count of these changes.
    Returns the dict storing tuples of (error,fix) keyed to counts.
    """
    freqs = {}
    with open(filename, 'rb') as f:
        rows = csv.reader(f, delimiter='\t', dialect='excel')
        for row in rows:
            if len(row) < 9:
                row.append("")
            errstr = re.sub(r'([.!,?]|""|~|\[no tag\])', ' ', row[7]).strip()
            fixstr = re.sub(r'([.!,?]|""|~|\[no tag\])', ' ', row[8]).strip()
            '''
            #this prints a report for nonmatching errs/fixes and mor tiers
            if errstr not in row[6]:
                if fixstr in row[6]:
                    #print("Error/fix swapped in row {} in {}".format(row[3], filename))
                    pass
                else:
                    print("Row {} in {} does not contain: ".format(row[3], filename))
                    print("\t[{}] OR [{}]\n".format(errstr, fixstr))
            #'''        
            errs = string_to_utterance(errstr)
            fixes = string_to_utterance(fixstr)
            if len(errs) != len(fixes):
                '''
                #this adds nonmatching edits into freqs with a negative count
                if ('+' not in str(errs) and '+' not in str(fixes)
                    and len(errs) > 0 and len(fixes) > 0):
                    freq_add(freqs, (str(errs), str(fixes)), -1)
                #'''
                pass
            else: 
                for i in range(len(errs)):
                    if errs[i].stem != fixes[i].stem:
                        t = (errs[i].stem, fixes[i].stem)
                        freq_add(freqs, t, 1)
                        #print filename + ": " + str(row[3]) + str(t)
    return freqs

#--
if __name__ == '__main__':
    if len(sys.argv) < 2:
        look_freqs(summed_dict(all_freqs()))
    elif len(sys.argv) == 2:
        look_freqs(stems_changed(sys.argv[1]))
    else:
        freqs_list = []
        for i in range(1, len(sys.argv)):
            freqs_list.append(stems_changed(sys.argv[i]))
        look_freqs(summed_dict(freqs_list))
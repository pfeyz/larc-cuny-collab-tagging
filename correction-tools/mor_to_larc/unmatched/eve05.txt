UNMATCHED TAGS
1109 .|. 	 {'lemma': '.', 'pos': '.'}
319 ?|? 	 {'lemma': '?', 'pos': '?'}
162 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
53 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
28 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
27 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
26 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
26 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
24 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
18 poss|s 	 {'lemma': 's', 'pos': 'poss'}
15 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
8 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
7 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
7 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
6 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
6 cop|be&1S 	 {'lemma': 'be', 'fusional_suffix': ['1S'], 'pos': 'cop'}
4 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
4 mod|genmod 	 {'lemma': 'genmod', 'pos': 'mod'}
3 conj|when 	 {'lemma': 'when', 'pos': 'conj'}
2 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
2 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
1 mod|could 	 {'lemma': 'could', 'pos': 'mod'}
1 mod|shall 	 {'lemma': 'shall', 'pos': 'mod'}
1 mod|must 	 {'lemma': 'must', 'pos': 'mod'}
1 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
1 cop|be&PAST 	 {'lemma': 'be', 'fusional_suffix': ['PAST'], 'pos': 'cop'}
1 conj|before 	 {'lemma': 'before', 'pos': 'conj'}
1 conj|so 	 {'lemma': 'so', 'pos': 'conj'}

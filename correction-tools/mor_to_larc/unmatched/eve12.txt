UNMATCHED TAGS
1006 .|. 	 {'lemma': '.', 'pos': '.'}
300 ?|? 	 {'lemma': '?', 'pos': '?'}
166 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
52 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
40 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
36 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
32 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
31 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
19 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
16 poss|s 	 {'lemma': 's', 'pos': 'poss'}
14 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
14 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
12 conj|because 	 {'lemma': 'because', 'pos': 'conj'}
10 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
9 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
7 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
6 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
5 coord|or 	 {'lemma': 'or', 'pos': 'coord'}
3 conj|when 	 {'lemma': 'when', 'pos': 'conj'}
3 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
2 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
2 conj|while 	 {'lemma': 'while', 'pos': 'conj'}
1 conj|before 	 {'lemma': 'before', 'pos': 'conj'}
1 mod|genmod 	 {'lemma': 'genmod', 'pos': 'mod'}
1 cop|be&PASTP 	 {'lemma': 'be', 'fusional_suffix': ['PASTP'], 'pos': 'cop'}
1 conj|as 	 {'lemma': 'as', 'pos': 'conj'}
1 conj|so 	 {'lemma': 'so', 'pos': 'conj'}
1 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
1 conj|until 	 {'lemma': 'until', 'pos': 'conj'}

UNMATCHED TAGS
1295 .|. 	 {'lemma': '.', 'pos': '.'}
417 ?|? 	 {'lemma': '?', 'pos': '?'}
246 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
125 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
74 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
49 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
41 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
39 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
32 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
25 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
23 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
16 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
15 conj|because 	 {'lemma': 'because', 'pos': 'conj'}
14 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
11 conj|when 	 {'lemma': 'when', 'pos': 'conj'}
11 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
11 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
8 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
8 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
8 conj|so 	 {'lemma': 'so', 'pos': 'conj'}
7 poss|s 	 {'lemma': 's', 'pos': 'poss'}
7 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
6 mod|genmod 	 {'lemma': 'genmod', 'pos': 'mod'}
6 conj|while 	 {'lemma': 'while', 'pos': 'conj'}
6 conj|until 	 {'lemma': 'until', 'pos': 'conj'}
5 coord|or 	 {'lemma': 'or', 'pos': 'coord'}
4 mod|must 	 {'lemma': 'must', 'pos': 'mod'}
3 conj|after 	 {'lemma': 'after', 'pos': 'conj'}
2 meta|huko 	 {'lemma': 'huko', 'pos': 'meta'}
2 cop|be&1S 	 {'lemma': 'be', 'fusional_suffix': ['1S'], 'pos': 'cop'}
2 mod|might 	 {'lemma': 'might', 'pos': 'mod'}
2 meta|bih 	 {'lemma': 'bih', 'pos': 'meta'}
1 meta|uh 	 {'lemma': 'uh', 'pos': 'meta'}
1 conj|before 	 {'lemma': 'before', 'pos': 'conj'}
1 mod:aux|ought_to 	 {'lemma': 'ought_to', 'pos': 'mod', 'subPos': ['aux']}
1 mod|should 	 {'lemma': 'should', 'pos': 'mod'}

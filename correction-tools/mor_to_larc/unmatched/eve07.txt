UNMATCHED TAGS
930 .|. 	 {'lemma': '.', 'pos': '.'}
289 ?|? 	 {'lemma': '?', 'pos': '?'}
176 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
45 poss|s 	 {'lemma': 's', 'pos': 'poss'}
43 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
31 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
24 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
20 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
20 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
15 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
12 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
9 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
7 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
6 conj|when 	 {'lemma': 'when', 'pos': 'conj'}
5 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
5 conj|so 	 {'lemma': 'so', 'pos': 'conj'}
4 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
3 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
2 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
2 conj|because 	 {'lemma': 'because', 'pos': 'conj'}
2 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
2 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
2 cop|be&1S 	 {'lemma': 'be', 'fusional_suffix': ['1S'], 'pos': 'cop'}
1 conj|while 	 {'lemma': 'while', 'pos': 'conj'}
1 conj|whether 	 {'lemma': 'whether', 'pos': 'conj'}
1 coord|or 	 {'lemma': 'or', 'pos': 'coord'}
1 conj|until 	 {'lemma': 'until', 'pos': 'conj'}
1 mod:aux|has_to 	 {'lemma': 'has_to', 'pos': 'mod', 'subPos': ['aux']}

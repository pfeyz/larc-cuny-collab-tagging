UNMATCHED TAGS
1081 .|. 	 {'lemma': '.', 'pos': '.'}
455 ?|? 	 {'lemma': '?', 'pos': '?'}
205 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
46 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
43 poss|s 	 {'lemma': 's', 'pos': 'poss'}
37 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
28 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
18 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
16 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
12 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
11 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
10 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
9 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
9 conj|because 	 {'lemma': 'because', 'pos': 'conj'}
9 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
8 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
7 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
5 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
3 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
3 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
2 mod|could 	 {'lemma': 'could', 'pos': 'mod'}
2 mod|genmod 	 {'lemma': 'genmod', 'pos': 'mod'}
2 cop|be&1S 	 {'lemma': 'be', 'fusional_suffix': ['1S'], 'pos': 'cop'}
1 mod|might 	 {'lemma': 'might', 'pos': 'mod'}
1 mod|should 	 {'lemma': 'should', 'pos': 'mod'}

UNMATCHED TAGS
1137 .|. 	 {'lemma': '.', 'pos': '.'}
270 ?|? 	 {'lemma': '?', 'pos': '?'}
130 cop|be&3S 	 {'lemma': 'be', 'fusional_suffix': ['3S'], 'pos': 'cop'}
48 mod|do 	 {'lemma': 'do', 'pos': 'mod'}
46 mod|will 	 {'lemma': 'will', 'pos': 'mod'}
41 coord|and 	 {'lemma': 'and', 'pos': 'coord'}
35 poss|s 	 {'lemma': 's', 'pos': 'poss'}
27 mod|can 	 {'lemma': 'can', 'pos': 'mod'}
25 -|- 	 {'lemma': '', 'pos': '-', 'suffix': ['']}
20 cop|be&PRES 	 {'lemma': 'be', 'fusional_suffix': ['PRES'], 'pos': 'cop'}
11 mod|will&COND 	 {'lemma': 'will', 'fusional_suffix': ['COND'], 'pos': 'mod'}
10 mod|do&PAST 	 {'lemma': 'do', 'fusional_suffix': ['PAST'], 'pos': 'mod'}
9 mod|may 	 {'lemma': 'may', 'pos': 'mod'}
8 cop|be 	 {'lemma': 'be', 'pos': 'cop'}
7 conj|but 	 {'lemma': 'but', 'pos': 'conj'}
6 conj|if 	 {'lemma': 'if', 'pos': 'conj'}
6 mod|do&3S 	 {'lemma': 'do', 'fusional_suffix': ['3S'], 'pos': 'mod'}
4 mod:aux|have_to 	 {'lemma': 'have_to', 'pos': 'mod', 'subPos': ['aux']}
3 mod:aux|got_to 	 {'lemma': 'got_to', 'pos': 'mod', 'subPos': ['aux']}
3 mod|shall 	 {'lemma': 'shall', 'pos': 'mod'}
3 conj|because 	 {'lemma': 'because', 'pos': 'conj'}
3 cop|be&1S 	 {'lemma': 'be', 'fusional_suffix': ['1S'], 'pos': 'cop'}
2 cop|be&PAST&13S 	 {'lemma': 'be', 'fusional_suffix': ['PAST', '13S'], 'pos': 'cop'}
1 conj|when 	 {'lemma': 'when', 'pos': 'conj'}
1 coord|or 	 {'lemma': 'or', 'pos': 'coord'}

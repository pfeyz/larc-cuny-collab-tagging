================================
 MOR tag to LARC tag conversion
================================

===========================
 Tagset Rewrite Rules File
===========================

A tagset rewrite rules file is a csv file of two columns, each row conceptually
expressing a rewrite rule ::

  pattern -> replacement

When an attempt is made to translate an input tag from the source (MOR) tagset
to the target (specified by the rules file), the following occurs:

- the rewrite rules file is parsed
- the rules are ordered from most specific to least specific. the more
  components a pattern has, the more specific a rule is, for example, "n:let-PL"
  is more specific than "n:let", since the former specifies a suffix in addition
  to the pos (n) and subPos (let) the latter specifies.
- the ordered patterns are iterated through one at a time until a pattern
  matches the input tag.
- when a match is found, the string "word/replacement" is generated
- if no match is found, "word/tag" is output, where tag is the pos component of
  the MOR tag, and an error is raised or the failure is logged to stdout.

Patterns
========

Each line of the rewrite rules csv file specifies a single rewrite rule to
attempt when translating a word from MOR to LARC tagsets. It uses a syntax that
resembles CHAT's mor tier part-of-speech clusters. For example ::

  pro:poss:det,det

can be thought of as ::

  PATTERN,REPLACEMENT

The pattern above, would match "your" when it appears in our corpus files as ::

  your/pro:poss:det|your

It would appear in the output with the replacement defined above::

  your/det

The pattern must have the following syntax::

  pos[:subPos1:subPos2...][|lemma][-suffix1-suffix2...][&fusionalSuffix1...]

where [brackets] denote optional components, ":", "|", "-" and "&" are literal
prefix characters. Thus the only required part of the pattern is the pos. A
minimal pattern could be ::

  n

while a maximal one could look like ::

  n:let|gurk-PL-DIM&3S

which would match a word with lemma "gurk" tagged as n:let in the third person
singular plural diminutive form. This is a nonsensical example.

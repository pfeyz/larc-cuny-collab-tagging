"""
MORtoLARC.py
2015/01/14

Translates an utterance from mor tags to larc tags.
First creates tag_dict from a word/MOR|word txt file.

args:
'f': name of txt file to read
returns:
a MOR tag dictionary.

Then return a list of (word, larc_tag) tuples.
"""

from __future__ import print_function
import re
import csv
from tag_to_dictionary import valid_mor_dict
from pyparsing_mor_to_dict import parse_tag, ParseException
import sys

from collections import Counter

class NoTranslationError(Exception):
    """ Raised when there is no matching translation rule for a tag """
    pass

def parse_rule_line(pattern):
    """Return a 2-tuple of (rule part count, mor token dictionary) from a tag pattern
    string.

    `rule part count` is literally just the number of pieces a rule has, used
    for sorting the rules in priority order, most specific rules are tried
    first.

    >>> parse_rule_line('n')
    (1, {'pos': 'n'})

    >>> parse_rule_line('v:cop&PAST')
    (3, {'pos': 'v', 'subPos': ['cop'], 'fusional_suffix': ['PAST']})

    >>> parse_rule_line('tag:f1:f2-S1-S2-S3&FS1&FS2')
    (8, {'pos': 'v',
    'subPos': ['f1', 'f2'],
    'suffix': ['S1', 'S2', 'S3'],
    'fusional_suffix': ['FS1', 'FS2']})

    """
    pattern_dict = {}
    parts = re.findall(r"(?:^|[:&-]|\|)\w+", pattern)
    priority = len(parts)
    pattern_dict['pos'] = parts[0]
    for part in parts[1:]:
        if part[0] == "|":
            pattern_dict['lemma'] = part[1:]
        elif part[0] == ";":
            pattern_dict[''] = part[1:]
            pattern_dict['word-form'] = part[1:]
        else:
            delims = {"&": "fusional_suffix",
                      "-": "suffix",
                      ":": "subPos"}
            for symbol, key in delims.items():
                if part[0] == symbol:
                    if key not in pattern_dict:
                        pattern_dict[key] = []
                    pattern_dict[key].append(part[1:])
    return (priority, pattern_dict)

def parse_tagset_rewrite_rules(filename):
    """Parses a tagset rewrite rule csv file.

    args:
    'filename': the name of the csv file to read.

    Returns a list of two-tuples. Each tuple contains a pattern dictionary as
    its first element, and a replacement pos tag string as its second. Each key
    in the pattern dictionary is the name of a MOR tag component (pos, subPos,
    lemma ...). The only required key is pos.

    the entries in filename must match the following regular
     expression: \w*?(:\w+?)*(&\w+?)*(-\w+?)*(\|\w+)?,\w+?
       aka
                 pos:subPos&fusional-suffix|wordform, newtag
       where all parts besides pos and |wordform can occur more than
       once, and newtag is arbitrary.

    """


    translation = []

    with open(filename) as csvfile:
        lines = csv.reader(csvfile)
        for pattern, newtag in lines:
            priority, pattern_dict = parse_rule_line(pattern)
            translation.append((priority, pattern_dict, newtag))

    return [(i[1], i[2]) for i in sorted(translation, key=lambda x: x[0], reverse=True)]

def translate_word(item, rewrite_rules):
    """Translates `item` (a tagged-word dictionary) and and equiv_dict, and returns
    a word/tag string according to the rules of the equiv_dict.

    """
    res = valid_mor_dict(item)
    assert res.valid, res.value

    word = item.get('lemma')
    tag = item

    if not tag:
        print('--', [item])
        return word + '/' + 'unk'

    for pattern, rewrite in rewrite_rules:
        match = True
        for key, val in pattern.items():
            match = (item.get(key) == val) and match
        if match:
            return word + '/' + rewrite
            break
    raise NoTranslationError('No translation for {}'.format(item))

def main(fn):

    """ Translates an utterance from mor tags to larc tags.
    First creates tag_dict from a word/MOR|word txt file.

    args:
    'f': name of txt file to read
    returns:
    a MOR tag dictionary.

    Then return a list of (word, larc_tag) tuples.

    """

    rewrite_rules = parse_tagset_rewrite_rules('tagset-rewrite-rules.csv')

    # store all the tags that had no matching rewrite rule
    failures = []
    with open(fn,'r') as f:
        for line in f:
            contents = line.split()
            uID = contents[0]
            speaker = contents[1]
            word_tags = line.split()[2:]
            print(speaker, end=' ')
            print(uID, end=' ')
            for word_tag in word_tags:
                try:
                    item = parse_tag(word_tag)
                except ParseException:
                    print('failed to parse tag at {}:{} - "{}"'.format(fn, uID, word_tag),
                          file=sys.stderr)
                    continue
                try:
                    newtag = translate_word(item, rewrite_rules)
                    print(newtag, end=' ')
                except NoTranslationError:
                    failures.append((word_tag, str(item)))
                    print('/'.join([item['lemma'], item['pos']]), end=' ')
                except AssertionError as e:
                    print('invalid tag parsed from {}:{}'.format(fn, uID), repr(e.args[0]),
                          file=sys.stderr)
                    print(e)
            print()

    print('Tags not handled by tagset conversion:', file=sys.stderr)
    for (tag, token), count in sorted(Counter(failures).items(), key=lambda x: x[1], reverse=True):
        print(count, tag, '\t', token, file=sys.stderr)
    print(file=sys.stderr)

# main('test.txt')


if __name__ == "__main__":
    main(sys.argv[1])

This file describe MOR string format and what it will be parsed into.

MOR tag format:
[prefix#]pos[:subPos1][:subPos2]...[:subPosN]|word[&fusion1]...[&fusionN][-suffix1]...[-suffixN]



Element		Symbol		Example		Representation		Part
(key)									(value)

1. pos				unwinding	un#v|wind-PRESP		‘v’
(part of speech)

2. subPos	after :		your		pro:poss:det|your	[‘poss’, ‘det’]

3. prefix	before #	unwinding	un#v|wind-PRESP		‘un’

4. lemma			unwinding	un#v|wind-PRESP		‘wind’

5. suffix 	after -		unwinding	un#v|wind-PRESP		‘PRESP’

6.fusional-suffix after &	mommy		adj|mom&dn-Y		’dn’

7. word-form			mommy		adj|mom&dn-Y		‘mommy’
# word-form can get from XML, but can not get from MOR

8. compound_parts		funny_looking	adj|+adj|funny+adj|looking
e.g. ’compound_parts': [{'pos': 'v', 'lemma': 'look'}, {'pos': 'pro', 'subPos': 'obj', 'lemma': 'it'}]

import unittest
from unittest import TestCase

from TranslateTag import (translate_word, parse_tagset_rewrite_rules,
    NoTranslationError)

class TagTranslationTests(TestCase):
    """ Tests the tagset translation process """

    def test_word_translation(self):
        """Tests that individual word/tag pairs are translated correctly.

        """
        # two-tuples of (match rules -> tag rewrite)
        rules = [
            # det:num-PL,det-pl
            ({'pos': 'det', 'subPos': ['num'], 'suffix': ['PL']},
             'det-pl'),

            ({'pos': 'fam', 'suffix': ['PL']},
             'chi-pl'), # not a real tag

            ({'pos': 'fam', 'subPos': ['hopping']},
             'jump-chi'),

            ({'pos': 'fam'},
             'chi')
        ]

        word = {'lemma': 'choo', 'pos': 'fam'}
        translated = translate_word(word, rules)
        self.assertEquals(translated, 'choo/chi')

        word = {'lemma': 'choo', 'pos': 'fam', 'suffix': ['PL']}
        translated = translate_word(word, rules)
        self.assertEquals(translated, 'choo/chi-pl')

        word = {'lemma': 'these', 'pos': 'det', 'subPos': ['num'],
                'suffix': ['PL']}
        translated = translate_word(word, rules)
        self.assertEquals(translated, 'these/det-pl')

        word = {'lemma': 'bop', 'pos': 'fam', 'subPos': ['hopping']}
        translated = translate_word(word, rules)
        self.assertEquals(translated, 'bop/jump-chi')

    def test_matches_fail(self):
        """Tests that specific rules don't overmatch less specific tags.

        For example, v&PAST matching run/v would be an error.

        """

        rules = [
            ({'pos': 'v', 'fusional_suffix': ['PAST']}, 'vbd'),
        ]

        self.assertRaises(NoTranslationError,
                          lambda: translate_word({'lemma': 'run', 'pos': 'v'}, rules))

        rules = [
            ({'pos': 'v', 'suffix': ['PAST']}, 'vbd'),
        ]

        self.assertRaises(NoTranslationError,
                          lambda: translate_word({'lemma': 'run', 'pos': 'v'}, rules))

        rules = [
            ({'pos': 'v', 'suffix': ['PAST']}, 'rule1'),
            ({'pos': 'v'}, 'rule2'),
        ]

        translation = translate_word({'lemma': 'run', 'pos': 'v'}, rules)
        self.assertEquals(translation, 'run/rule2')

    def test_ruleset_generation(self):
        """ Test generating translation rules from a csv file """
        fixture_file = 'test-fixtures/translation-rules.csv'
        generated_rules = parse_tagset_rewrite_rules(fixture_file)
        expected_rules = [
            ({'pos': 'tag', 'subPos': ['F1', 'F2'],
              'suffix': ['ALPHA', 'BRAVO'], 'fusional_suffix': ['CHARLIE', 'DELTA']},
             'newtag'),
            ({'subPos': ['let'], 'pos': 'n', 'suffix': ['PL']}, 'let-pl'),
            ({'subPos': ['num'], 'pos': 'det'}, 'det'),
            ({'subPos': ['adj'], 'pos': 'n'}, 'n'),
            ({'subPos': ['loc'], 'pos': 'adv'}, 'loc'),
            ({'subPos': ['n'], 'pos': 'v'}, 'vb'),
            ({'subPos': ['v'], 'pos': 'adj'}, 'adj'),
            ({'pos': 'v', 'fusional_suffix': ['PAST']}, 'vbd'),
            ({'pos': 'sing'}, 'sin'),
            ({'pos': 'bab'}, 'vb'),
            ({'pos': 'neo'}, 'dashNEO'),  # what is this?
            ({'pos': 'no'}, 'ptl')]
        self.assertEquals(generated_rules, expected_rules)

if __name__ == "__main__":
   unittest.main()

import unittest

import correction_tools
from correction_tools import apply_correction, Correction, remove_unknowns, RemoveUtterance
from talkbank_parser import MorToken

def simple_correction(error, replacement):
    return Correction(user=None, corpus=None, filename=None, uid=None,
                      speaker=None, speaker_tier=None, mor_tier=None,
                      error=error, replacement=replacement, notes=None)

def string_to_utterance(string):
    """Convenience function that converts a space-separated string of mor-style
    tag|lemma tokens into a list of MorTokens.

    """
    return [MorToken.from_string(word)
            for word in string.split()]

class CorrectionTests(unittest.TestCase):
    def test_simple_correction(self):
        utterance = string_to_utterance(
            'qn|all det|the v|water conj:coo|and det|the n|broom-PL')
        correction = simple_correction(error='v|water', replacement='n|water')
        apply_correction(correction, (None, None, utterance))
        self.assertEqual(utterance, string_to_utterance(
            'qn|all det|the n|water conj:coo|and det|the n|broom-PL'))

    def test_reduce_word(self):
        utterance = string_to_utterance(
            'would/aux|will&COND you/pro|you like/v|like it/pro|it '
            'in/prep|in your/pro:poss:det|your tea/n|tea cup/n|cup ?/?|?')
        correction = simple_correction(error='n|tea n|cup',
                                       replacement='n|+n|tea+n|cup')
        apply_correction(correction, (None, None, utterance))
        self.assertEqual(utterance, string_to_utterance(
            'would/aux|will&COND you/pro|you like/v|like it/pro|it '
            'in/prep|in your/pro:poss:det|your n|+n|tea+n|cup ?/?|?'))

    def test_expand_word(self):
        utterance = string_to_utterance('see/v|see the/det|the butterfly/n|butterfly')
        correction = simple_correction(error='butterfly/n|butterfly',
                                       replacement='butter/n|butter fly/v|fly')
        apply_correction(correction, (None, None, utterance))
        self.assertEqual(utterance, string_to_utterance(
            'see/v|see the/det|the butter/n|butter fly/v|fly'))

    def test_missing_target(self):
        utterance = string_to_utterance('see/v|see the/det|the')
        correction = simple_correction(error='butterfly/n|butterfly',
                                       replacement='butter/n|butter fly/v|fly')
        with self.assertRaises(correction_tools.TokenNotFound):
            apply_correction(correction, (None, None, utterance))

    def test_multiple_target(self):
        utterance = string_to_utterance('see/v|see the/det|the butterfly/n|butterfly butterfly/n|butterfly')
        correction = simple_correction(error='butterfly/n|butterfly',
                                       replacement='butter/n|butter fly/v|fly')
        with self.assertRaises(correction_tools.MultipleFound):
            apply_correction(correction, (None, None, utterance))

    def test_already_corrected(self):
        utterance = string_to_utterance('see/v|see the/det|the butter/n|butter fly/v|fly')
        correction = simple_correction(error='butterfly/n|butterfly',
                                       replacement='butter/n|butter fly/v|fly')
        with self.assertRaises(correction_tools.AlreadyCorrected):
            apply_correction(correction, (None, None, utterance))

    def test_not_found(self):
        utterance = string_to_utterance('see/v|see the/det|the butter/n|butter fly/v|fly')
        correction = Correction(user=None, corpus=None, filename=None, uid=None,
                                speaker='MOT', speaker_tier=None, mor_tier=None,
                                error='a|b', replacement='c|d', notes=None)
        correction = simple_correction(error='butterfly/n|butterfly',
                                       replacement='butter/n|butter fly/v|fly')
        with self.assertRaises(correction_tools.UtteranceNotFound):
            apply_correction(correction, (None, 'FAT', utterance))

class TransformationTests(unittest.TestCase):
    def test_remove_unknown_utts(self):
        """Tests that remove_unknowns correctly drops unknown tokens or deletes
        entire """

        #no unks, should return utterance unmolested
        utterance = string_to_utterance("do/mod|do you/pro|you want/v|want it/pro|it ?/?|?")
        self.assertEqual(remove_unknowns(utterance), utterance)
        
        utterance = string_to_utterance("yeah/co|yeah ./.|.")
        self.assertEqual(remove_unknowns(utterance), utterance)
        
        utterance = string_to_utterance("all/adv:int|all gone/part|go&PERF ./.|.")
        self.assertEqual(remove_unknowns(utterance), utterance)

        #full unks, should return RemoveUtterance
        utterance = string_to_utterance("deletme/unk|deleteme ./.|.")
        self.assertEqual(remove_unknowns(utterance), RemoveUtterance)
        
        utterance = string_to_utterance("xx/unk|deleteme deletme/unk|deleteme xx/unk|deleteme")
        self.assertEqual(remove_unknowns(utterance), RemoveUtterance)
        
        utterance = string_to_utterance("deletme/unk|deleteme deletme/unk|deleteme ?/?|?")
        self.assertEqual(remove_unknowns(utterance), RemoveUtterance)
        
        #partial unks, should return an altered utterance
        utterance = string_to_utterance("an/det|a deletme/unk|deleteme in/prep|in your/pro:poss:det|your kitchen/n|kitchen ?/?|?")
        altered_utt = string_to_utterance("an/det|a in/prep|in your/pro:poss:det|your kitchen/n|kitchen ?/?|?")
        self.assertEqual(remove_unknowns(utterance), altered_utt)
        
        utterance = string_to_utterance("xx/unk|deleteme like/prep|like that/pro:dem|that xx/unk|deleteme ./.|.")
        altered_utt = string_to_utterance("like/prep|like that/pro:dem|that ./.|.")
        self.assertEqual(remove_unknowns(utterance), altered_utt)
        
        utterance = string_to_utterance("what/pro:wh|what it/pro|it is/v:cop|be&3S xx/unk|deleteme those/pro:dem|those ?/?|?")
        altered_utt = string_to_utterance("what/pro:wh|what it/pro|it is/v:cop|be&3S those/pro:dem|those ?/?|?")
        self.assertEqual(remove_unknowns(utterance), altered_utt)

if __name__ == '__main__':
    unittest.main()

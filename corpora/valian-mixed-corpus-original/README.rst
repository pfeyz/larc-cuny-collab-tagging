======================
 Mixed Corpus Sources
======================

These are the sources for the corpora in this directory ::

    tagger-evaluation-docs-dropbox_xml-converted-from-cha/01a.xml
    tagger-evaluation-docs-dropbox_xml/01b.xml
    tagger-evaluation-docs-dropbox-jan-2013_cha-xml/02a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/02b.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/03a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/03b.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/04a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/04b.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/04c.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/05a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/06a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/06b.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/07a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/08a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/08b.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/15a.xml
    tagger-evaluation-docs-dropbox_xml-converted-from-cha/15b.xml

# split.py
# 20150228
# 20160523 updated, delete blank line from inputfile

import sys

"""
    split WORD/LARCtag TXT files into children's speech
and adults' speech.
    args:
    f: WORD/LARCtag TXT files
    CHI: children's speech file
    ADU: adults' speech file

"""

def split(f):
    with open(f) as infile, open('CHI' + '.txt', 'w') as outfile, open('ADU' + '.txt', 'w') as outtfile:
        for line in infile:
            contents = line.split()
            length = len(contents[2:])
            if length > 1:
                uID = contents[1]
                speaker = contents[0]
                words = contents[2:]
                if speaker == 'CHI':
                    outfile.writelines(' '.join(contents)+'\n')
                else:
                    outtfile.writelines(' '.join(contents)+'\n')
                  # words to contents

    outfile.close()
    outtfile.close()




if __name__ == "__main__":
	split(sys.argv[1])


# split('allValian.txt','CHI.txt','ADU.txt')



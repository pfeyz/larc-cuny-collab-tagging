# stratified_sampling.py
# 20150402
# 20160530 update, adding training files

import random
from collections import defaultdict
from sys import argv


def stratified_sampling(f):

	"""Dividing utterances of the corpura into subgroups of same length,
	then split each subgroups into 10 fold,
	collect residuals into a list 'R',
	finially randomly split R to 10 parts.

	argv: tagged files, one utterance per line.
	return: 10 files, each contain 1/10 of the original file.
	"""

	with open(f) as inputfile:
		for i, l in enumerate(inputfile):
			pass
	num_line = (i + 1)/10

	# utterances = []
	DD = defaultdict(list)
	with open(f) as inputfile:
		for line in inputfile.readlines():
			word_list = line.split()
			length = len(word_list[2:])
			if length > 1:
				sentence = " ".join(word_list[2:])
				# print type(sentence) is str
				# utterances.append(sentence+'\n')
				DD[length].append(sentence+'\n')

	residual = []
	for i in DD.keys():
		sents = DD.get(i)
		random.shuffle(sents)
		sents_num = len(sents)/10
		for i in range(10):
			testing_file = open('test' + str(i) + '.txt','ab')
			testing = sents[(i * sents_num) : ((i+1) * sents_num)]
			for line in testing:
				testing_file.write(line)
			testing_file.close()
		for sent in sents[(sents_num*10):]:
			residual.append(sent)

	sent_num = len(residual)/10
	random.shuffle(residual)
	for i in range(10):
		testing_file = open('test' + str(i) + '.txt','ab')
		testing = sents[(i * sent_num) : ((i+1) * sent_num)]
		for line in testing:
			testing_file.write(line)
		testing_file.close()

	for i in range(10):
		training_file = open('train' + str(i) + '.txt', 'w')
		for j in range(10):
			if j != i:
				with open('test' + str(j) + '.txt','r') as infile:
					file_content = infile.readlines()
					for line in file_content:
						training_file.writelines(line)
		training_file.close()



# stratified_sampling()

if __name__ == "__main__":
	stratified_sampling(argv[1])


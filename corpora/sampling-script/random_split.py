# random_split.py
# randomly split data into 10 testing/training set for 10 fold cross validation
# 20150401
# 20160527 updated, delete blank line from inputfile

from sys import argv
import random

def random_split(f):
	n = 0
	with open(f) as inputfile:
		file_content = inputfile.readlines()
		for line in file_content:
			contents = line.split()
			length = len(contents[2:])
			if length > 1:
				n += 1
	num_line = (n + 1)/10


	utterances = []
	with open(f) as inputfile:
		file_content = inputfile.readlines()
		random.shuffle(file_content)
		for line in file_content:
			word = line.split()
			sentence = " ".join(word[2:])
			if sentence:
				utterances.append(sentence+'\n')

	for i in range(10):
		testing = utterances[(i * num_line) : ((i+1) * num_line)]
		training = utterances[:i*num_line] + utterances[(i+1)*num_line:]
		testing_file = open('test' + str(i) + '.txt','w')
		testing_file.writelines(testing)
		testing_file.close()
		training_file = open('train' + str(i) + '.txt', 'w')
		training_file.writelines(training)
		testing_file.close()

if __name__ == "__main__":
	random_split(argv[1])

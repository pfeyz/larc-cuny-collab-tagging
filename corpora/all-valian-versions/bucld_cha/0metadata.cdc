@UTF8
Title:	English Valian Corpus
Creator:	Valian, Virginia
Subject:	child language development
Subject.olac:linguistic-field:	language_acquisition
Subject.olac:language:	en
Subject.childes:participant:	age="1;9.20 - 2;8.24"
Description:	compares English and Italian development in regard to subject deletion
Publisher:	TalkBank
Contributor:	
Date:		2004-03-30
Type:	Text
Type.olac:linguistic-type:	primary_text
Type.olac:discourse-type:	dialogue 
Format:		 
Identifier:	1-59642-033-2
Language:	
Relation:
Coverage:
Rights:

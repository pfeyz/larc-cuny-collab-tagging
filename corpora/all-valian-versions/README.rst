=========================================
 Different Versions of the Valian Corpus
=========================================

(* indicates a non-archival derived corpus; a corpus that was converted from one
format to another after the establishment of this repo)

bucld_cha (March 2011)

  From a subversion repo of a project I was working on with Erin for the Boston
  University Child Language Development conference. The corpus was checked into
  the svn repo on March 23rd, 2011.

*bucld_cha-xml

  Converted from bucld_cha November 2015.

splits-for-caitlin_cha (2011?)

  From an old project where I split the corpus into pieces for intern Caitlin
  Richter during Summer 2011.

*splits-for-caitlin_cha-xml

  Converted from the cha counterpart in Nov 2015.

lucia-regex_cha (Summer 2011?)

  From a project I was working on for Lucia Pozzan. Last commit in the codebase
  is from August 2011, but unfortunately the corpus wasn't version controlled,
  so it could be newer than that.

*lucia-regex_cha-xml

  Converted from the cha counterpart in Nov 2015.

tagchecker-email-from-linda-liu_xml (2012)

  Found as an attachment from linda liu in July 2012.

manchester-dropbox_cha (mostly June 2012, part Oct 2013)

  Found in the "Manchester (1)" dropbox folder, in the Valian_Corpus/0_Original
  directory. Last modified date in the dir list view is June 5th 2012, but
  "Version history" claims that 01a and 20b were edited by Cindy in October
  of 2013.

*manchester-dropbox_cha-xml

  Converted from the cha counterpart in Nov 2015.

django-app-versioned_cha (March 2013)

  From the repo for the second-check correction process. Corpus was added to
  repo on March 20th, 2013.

*django-app-versioned_cha-xml

  Converted from its cha counterpart in Nov 2015.

tagchecker-dropbox_xml (June 2012? Jan 2013?)

  Found in the dropbox where tagchecker was run from by the
  annotators. Dropbox's last modified dates conflict between the directory list
  view and the file history view. The directory list view says the files were
  last modified by Chris Wegenaar June 17, 2012, but the "Previous Versions"
  view shows the initial version of the file was added by Chris Jan 21, 2013.

tagger-evaluation-docs-dropbox-jan-2013_cha (Jan 2013? May 2014?)

  Found in the "Tagger Evaluation Docs" folder on Dropbox. The folder name
  indicates the corpus is from Jan 14th, 2013, which the Dropbox date modified
  field corroborates, but once again the "Previous Versions" view indicates that
  Version 1 was created May 22, 2014...

*tagger-evaluation-docs-dropbox-jan-2013_cha-xml

  Converted from the cha counterpart in Nov 2015.

tagger-evaluation-docs-dropbox_cha, tagger-evaluation-docs-dropbox_xml,
tagger-evaluation-docs-dropbox_xml-converted-from-cha (Summer 2014)

  Found in the "Tagger Evaluation docs" folder on dropbox. Dropbox says these
  files were added by Rui in Summer 2014.

django-app-unversioned_cha (< August 2014)

  From the second-check correction process source folder, but was never checked
  into the repo. Filesystem last-modifed date is August 2014

*django-app-unversioned_cha-xml

  Converted from django-app-unversioned_cha in October 2015.

cuny-collab-repo_cha, cuny-collab-repo_xml (?)

  Stored in the cuny collab tagging project repo. Was believed to have come from
  the original tagchecker repo. I reran chatter to make sure that the xml came
  from the cha, and they are the same except that the xml version has commas in
  it.

tagchecker-old_xml (?)

  From a copy of the tagchecker source I have on my local disk.

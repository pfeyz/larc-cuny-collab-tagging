# remove all commas

dir=$1

for corpus_file in $dir/*.cha
do
    # remove commas, leaving the header files alone (first 5 lines)
    sed -ri '5,$s/,//g' $corpus_file

    # convert unk tags to 'deleteme'
    sed -ri 's#unk\|\w+#unk|deleteme#g' $corpus_file
    sed -ri 's#www|xxx|yyy#deletme#g' $corpus_file

    # convert language code from 'en' to 'eng'
    sed -ri 's/\ten[^g]/\teng/' $corpus_file

    # append blank field to the end of the @ID metadata line
    # sed -ri 's/(^@ID:.*$)/\1|/' $corpus_file

    # remove + from the beginning of utterances
    sed -ri 's/\t\+\W?/\t/' $corpus_file

    sed -ri 's#<.*?> \[//?\]##g' $corpus_file
    sed -ri 's#\S*[^>] \[//?\]##g' $corpus_file
    # sed -ri 's/<.*?>//g' $corpus_file

    # remove [x] and [xx] annotation (x is variable)
    perl -pi -e 's/\[..?\]//g' $corpus_file

    # remove [= ...] and [% ...]
    perl -pi -e 's/\[[=%].*?\]//g' $corpus_file

    # remove remaining <...> phrases
    sed -ri 's/[<>]//g' $corpus_file

    # mark for deletion utterances that only contain punctuation
    sed -ri 's/^(\*[A-Z0-9]+):(\s|[.?!])+$/\1:	deleteme ./' $corpus_file


done

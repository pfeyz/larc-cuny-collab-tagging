"""Split a correction TSV up into multiple files.

This script takes a single argument, the filename of a tab-separated-values
file, and distributes this file's contents across multiple new files,
named/grouped by the FILENAME column in the TSV.

"""

import csv
import re
import sys

FILENAME_COL = 2
CORPUS_COL = 1

def main(filename):
    seen = set()
    with open(filename) as handle:
        reader = csv.reader(handle, dialect='excel', delimiter='\t')
        first = True
        for line in reader:
            if first:
                first = False
                continue
            if len(line) < 8:
                print('bad line', line)
                continue
            str_line = '\t'.join(line)
            if str_line in seen:
                continue
            seen.add(str_line)
            out_fn = re.sub(r'\..*$', '', line[FILENAME_COL])
            with open(out_fn, 'a') as outfile:
                outfile.write(str_line + '\n')

if __name__ == "__main__":
    try:
        FILENAME = sys.argv[1]
        main(FILENAME)
    except IndexError:
        print(""" usage: split_tsv_to_files.py all_corrections.tsv
one argument required.""")

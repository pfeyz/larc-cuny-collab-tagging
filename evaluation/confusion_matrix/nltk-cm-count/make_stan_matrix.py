# make_matrix.py
# make Confusion Matrices from tagged file and gold standard
# 2015-03-04

import nltk
import re
import sys

def tag_list(tagged_file):
	p = re.compile(r"/(\w+?) ")
	tag_list = []

	with open(tagged_file, 'r') as infile:
		for line in infile:
			tags = re.findall(p, line)
			# print tags
			for tag in tags:
				# print tag
				tag_list.append(tag)
		return tag_list
		# print tag_list

# tag_list('test0.txt')


def matrix(gold, test, cm_chart):
	gold = tag_list(gold)
	test = tag_list(test)
	# print gold
	# print test
	cm = nltk.ConfusionMatrix(gold, test)
	with open(cm_chart, 'w') as outfile:
		outfile.write(cm.pp(sort_by_count=True))


# matrix('test0.txt', 'test0.stanfordtagged.txt', 'cm.txt')

if __name__ == "__main__":
	matrix(sys.argv[1], sys.argv[2], sys.argv[3])



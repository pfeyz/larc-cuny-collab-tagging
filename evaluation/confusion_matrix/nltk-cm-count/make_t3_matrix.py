# make_t3_matrix.py
# make Confusion Matrices from t3 tagged file and gold standard
# 2015-03-14

import nltk
import re
import sys

def tag_list(tagged_file):
	p = re.compile(r"/(\w+?) ")
	tag_list = []

	with open(tagged_file, 'r') as infile:
		for line in infile:
			tags = re.findall(p, line)
			for tag in tags:
				tag_list.append(tag)
		return tag_list
		# print tag_list
		# print len(tag_list)


# tag_list('test0.txt')

def tagged_tag_list(t3_tagged_file):
	tag_list = []
	with open(t3_tagged_file, 'r') as infile:
		for line in infile:
			for i in range(0, len(line.split()), 2):
				if re.match("\w", line.split()[i+1]):
					tag_list.append(line.split()[i+1])
		return tag_list
		# print tag_list
		# print len(tag_list)

# tagged_tag_list('test0.t3tagged.txt')

def matrix(gold, test, cm_chart):
	gold = tag_list(gold)
	test = tagged_tag_list(test)
	# print gold
	# print test
	cm = nltk.ConfusionMatrix(gold, test)
	with open(cm_chart, 'w') as outfile:
		outfile.write(cm.pp(sort_by_count=True))


# matrix('test0.txt', 'test0.t3tagged.txt', 'cm.txt')

if __name__ == "__main__":
	matrix(sys.argv[1], sys.argv[2], sys.argv[3])



# make_matrix.py
# make Confusion Matrices from tagged file and gold standard
# 2015-03-04
# add precision and recall to confusion matrix
# 2015-03-20
# add up by rows and by colums
# 2015-06-03

import nltk
import numpy as np
import re
import sys
import csv
import pprint

""" 
	This script makes confusion matrix. Produces a list of lists contain
	word tags, tagging counts, precision of each tag, and recall of each tag.

	argv: 
		gold: gold standard file in word/tag format;
		test: computational tagged file in word/tag format;
		f : output csv file name.


"""

def tag_list(tagged_file):
	# p = re.compile(r"/(\w+?) ")
	p = re.compile(r"/([-\w]*\w) ")
	tag_list = []

	with open(tagged_file, 'r') as infile:
		for line in infile:
			tags = re.findall(p, line)
			for tag in tags:
				tag_list.append(tag)
		return tag_list

# tag_list('test0.txt')


def matrix(gold, test):
	gold = tag_list(gold)
	test = tag_list(test) 
	cm = nltk.ConfusionMatrix(gold, test)
	return cm
	# print cm
	# print cm.pp(sort_by_count=True)

# matrix('test.txt', 'tagged.txt')

def precision(gold, test):
	# pp = pprint.PrettyPrinter(depth=6)
	m = matrix(gold, test)
	precision_list = []
	# pp.pprint(m._confusion)
	for count_list in m._confusion:
		p_max_count = max(count_list)
		p_all_count = sum(count_list)
		precision = p_max_count * 100/float(p_all_count)
		precision_list.append(round(precision, 2))

	return precision_list
	# pp.pprint(precision_list)
	# pp.pprint(m._confusion)
	
# precision('test.txt', 'tagged.txt')	


def precision_and_recall(gold, test, output_f):
	m = matrix(gold, test)
	# np.set_printoptions(precision=2)

		
	matrix_array = np.array(m._confusion)
	max_p_count = matrix_array.max(axis=1)
	total_p = matrix_array.sum(axis = 1)
	precision = np.divide(max_p_count*100, total_p, dtype = float)
	# print matrix_array

	for count_list in m._confusion:
		count_list.append(sum(count_list))
	sum_matrix_array= np.array(m._confusion)
	# print sum_matrix_array

	max_r_count = matrix_array.max(axis = 0)
	total_r = matrix_array.sum(axis = 0)
	recall = np.divide(max_r_count*100 , total_r, dtype = float)
	total_r_list = total_r.tolist()
	recall_list = recall.tolist()


	data = m._confusion
	# print m._confusion

	for index, row in enumerate(data):
		row.append(round(precision[index],2))
	data.append([ elem for elem in total_r_list])
	data.append([ '%.2f' % elem for elem in recall_list])

	reference_tag_list = m._values
	reference_tag_list.append("Total")
	reference_tag_list.append("Recall")

	for index, row in enumerate(data):
		if index < len(data):
			row.insert(0, reference_tag_list[index])

	reference_tag_list.insert(0,'TAG')
	reference_tag_list[-1] = 'Precision'
	# reference_tag_list.append('Precision')
	data.insert(0, reference_tag_list)
	# print np.array(data)

	with open(output_f, 'wb') as f:
		writer = csv.writer(f)
		writer.writerows(data)

# precision_and_recall('test.txt', 'tagged.txt', 'test.csv')

if __name__ == "__main__":
	precision_and_recall(sys.argv[1], sys.argv[2], sys.argv[3])
